create_clock -name {clk} -period 3.2 [ get_ports { clk } ]

set_false_path -to [ get_pins { **/ALn } ]
