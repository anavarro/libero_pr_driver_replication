--
-- Synopsys
-- Vhdl wrapper for top level design, written on Wed Jan 20 11:21:41 2021
--
library ieee;
use ieee.std_logic_1164.all;

entity wrapper_for_top is
   port (
      clk : in std_logic;
      rst : in std_logic;
      sw : out std_logic;
      ne : out std_logic
   );
end wrapper_for_top;

architecture behavioral of wrapper_for_top is

component top
 port (
   clk : in std_logic;
   rst : in std_logic;
   sw : out std_logic;
   ne : out std_logic
 );
end component;

signal tmp_clk : std_logic;
signal tmp_rst : std_logic;
signal tmp_sw : std_logic;
signal tmp_ne : std_logic;

begin

tmp_clk <= clk;

tmp_rst <= rst;

sw <= tmp_sw;

ne <= tmp_ne;



u1:   top port map (
		clk => tmp_clk,
		rst => tmp_rst,
		sw => tmp_sw,
		ne => tmp_ne
       );
end behavioral;
