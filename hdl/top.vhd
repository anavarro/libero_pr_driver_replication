library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity top is
  port (
    clk : in  std_logic;
    rst : in  std_logic;
    sw  : out std_logic;
    ne  : out std_logic
    );
end top;

architecture behavioral of top is

  -- For future tests with TMR
  attribute syn_radhardlevel : string;
  attribute syn_radhardlevel of behavioral: architecture is "none";

  -- SIGNALS
  -- global routing
  signal gclk, grst: std_logic;
  
  -- constrained to center of fabric
  signal center: std_logic;
  
  -- "bridge" between center an corners
  signal free_common : std_logic;
  
  -- constrained to their respective corners of fabric
  signal sw_ff, ne_ff : std_logic_vector(63 downto 0);

  
  -- Prevent registers optimization
  attribute syn_preserve : boolean;
  attribute syn_preserve of ne_ff: signal is true;
  attribute syn_preserve of sw_ff: signal is true;

  component CLKINT_PRESERVE
  port(
    A     : in std_logic;
    Y     : out std_logic
    );
  end component;


begin 

  clkbuf : CLKINT_PRESERVE
  port map(A => clk, Y => gclk );

  rstbuf : CLKINT_PRESERVE
  port map(A => rst, Y => grst );

  process(gclk,grst)
  begin
    if grst = '1' then
      center    <= '1';
    elsif rising_edge(gclk) then
      center    <= '0';
    end if;
  end process;
  
  process(gclk)
  begin
    if rising_edge(gclk) then
      free_common <= center;
    end if;
  end process;
  
  process(gclk)
  begin
    if rising_edge(gclk) then
      if free_common = '1' then ne_ff <= (others => '0');
      else                      ne_ff <= '1' & ne_ff(ne_ff'high downto 1);
      end if;
      if free_common = '1' then sw_ff <= (others => '0');
      else                      sw_ff <= '1' & sw_ff(sw_ff'high downto 1);
      end if;
    end if;
  end process;

  sw <= sw_ff(0);
  ne <= ne_ff(0);

end behavioral;