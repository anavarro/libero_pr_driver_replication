set_device -family {PolarFire} -die {MPF300TS} -speed {-1}
read_adl {c:\libero_test_pr_driver_replication\designer\top\top.adl}
read_afl {c:\libero_test_pr_driver_replication\designer\top\top.afl}
map_netlist
read_sdc {c:\libero_test_pr_driver_replication\constraint\user.sdc}
check_constraints {c:\libero_test_pr_driver_replication\constraint\timing_sdc_errors.log}
write_sdc -mode smarttime {c:\libero_test_pr_driver_replication\designer\top\timing_analysis.sdc}
