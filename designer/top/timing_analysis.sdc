# Microsemi Corp.
# Date: 2021-Jan-20 11:23:23
# This file was generated based on the following SDC source files:
#   c:/libero_test_pr_driver_replication/constraint/user.sdc
#

create_clock -name {clk} -period 3.2 [ get_ports { clk } ]
set_false_path -to [ get_pins { **/ALn } ]
