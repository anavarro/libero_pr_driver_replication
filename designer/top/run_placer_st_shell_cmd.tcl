read_sdc -scenario "place_and_route" -netlist "optimized" -pin_separator "/" -ignore_errors {c:/libero_test_pr_driver_replication/designer/top/place_route.sdc}
set_options -tdpr_scenario "place_and_route" 
save
set_options -analysis_scenario "place_and_route"
report -type combinational_loops -format xml {c:\libero_test_pr_driver_replication\designer\top\top_layout_combinational_loops.xml}
report -type slack {c:\libero_test_pr_driver_replication\designer\top\pinslacks.txt}
set coverage [report \
    -type     constraints_coverage \
    -format   xml \
    -slacks   no \
    {c:\libero_test_pr_driver_replication\designer\top\top_place_and_route_constraint_coverage.xml}]
set reportfile {c:\libero_test_pr_driver_replication\designer\top\coverage_placeandroute}
set fp [open $reportfile w]
puts $fp $coverage
close $fp