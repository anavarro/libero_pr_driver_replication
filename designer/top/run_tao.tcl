set_device -family {PolarFire} -die {MPF300TS} -speed {-1}
read_vhdl -mode vhdl_2008 {c:\libero_test_pr_driver_replication\hdl\top.vhd}
set_top_level {top}
map_netlist
check_constraints {c:\libero_test_pr_driver_replication\constraint\synthesis_sdc_errors.log}
write_fdc {c:\libero_test_pr_driver_replication\designer\top\synthesis.fdc}
